# Copyright 2011 Dan Callaghan <djc@djc.id.au>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools test=nose ]

SUMMARY="Python library for working with RDF"
DESCRIPTION="
RDFLib is a Python library for working with RDF, a simple yet powerful language for representing
information. The library contains parsers and serializers for RDF/XML, N3, NTriples, Turtle, TriX
and RDFa. The library presents a Graph interface which can be backed by any one of a number of store
implementations, including, memory, MySQL, Redland, SQLite, Sleepycat, ZODB and SQLObject.
"
HOMEPAGE+=" https://github.com/RDFLib/${PN}"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"

DEPENDENCIES="
    run+test:
        dev-python/html5lib[python_abis:*(-)?]
        dev-python/isodate[python_abis:*(-)?]
        dev-python/SPARQLWrapper[python_abis:*(-)?]
"

BUGS_TO="djc@djc.id.au"

# Tests fetch sample data from the network
# https://github.com/RDFLib/rdflib/issues/269
RESTRICT="test"

setup-py_test_one_multibuild() {
    case ${MULTIBUILD_TARGET} in
    2*)
        edo ${PYTHON} -B run_tests.py
        ;;
    3*)
        # Tests have to be run from the build/src directory, since they have to go
        # through 2to3 conversion before they'll work in python3.
        edo pushd build/src
        edo ${PYTHON} -B run_tests.py
        edo popd
        ;;
    esac
}
