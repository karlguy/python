# Copyright 2009 Daniel Mierswa <impulze@impulze.org>
# Copyright 2010 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist=3 ]

SUMMARY="Python wrapper for OpenSSL."
DESCRIPTION="
The most complete Python wrapper for OpenSSL featuring RSA, DSA, DH, HMACs,
message digests, symmetric ciphers (including AES); SSL functionality to
implement clients and servers; HTTPS extensions to Python's httplib, urllib,
and xmlrpclib; unforgeable HMAC'ing AuthCookies for web session management;
FTP/TLS client and server; S/MIME; ZServerSSL: A HTTPS server for Zope and
ZSmime: An S/MIME messenger for Zope. M2Crypto can also be used to provide SSL
for Twisted.
"
HOMEPAGE+=" https://gitlab.com/m2crypto/m2crypto"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# NOTE(mixi): typing is included in python starting with 3.5
DEPENDENCIES="
    build+run:
        dev-lang/swig[>=2.0.4]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        python_abis:2.7? ( dev-python/typing[python_abis:2.7] )
"

SETUP_PY_SRC_COMPILE_PARAMS=(
    --openssl=/usr/$(exhost --target)
)

# NOTE(moben): I hate upstream.
# https://github.com/M2Crypto/M2Crypto / http://chandlerproject.org/Projects/MeTooCrypto says that
# https://github.com/martinpaljak/M2Crypto is the official repo.
# https://github.com/martinpaljak/M2Crypto says that the former is.
# Both don't match the code in the release _and_ the relese is missing the test(s) directory that is
# present in both.
# (also some sandbox violations, but those are easily fixable)
#
# NOTE(kepstin): Now some of the issues in martinpaljak/M2Crypto have been
# closed, saying that they're fixed in a new repository
# https://gitlab.com/m2crypto/m2crypto
# This one includes the fix for SWIG >= 3.0.5, and is the basis for the latest
# packages published on PyPI
#
# NOTE(mixi): Apparently the tests (excluding the sandbox violations and maybe one or two failures
# I overlooked between all the sandbox errors) work now. Most of them are caused by s_server which
# apparently has no option to bind to anything but INADDR_ANY. Also the upstream git repo confusion
# seems to have settled and https://gitlab.com/m2crypto/m2crypto is the official one. Most of the
# other sources moben mentioned don't exist anymore.
#
# Last checked: 0.26.0
RESTRICT='test'

